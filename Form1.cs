﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace MultiscaleModeling
{
    public partial class MultiscaleModelingApp : Form
    {
        private bool enableGenerateNucleiButton = false;
        private MemoryCache memoryCache;
        private UserInteraction userInteraction;
        private TimerInteraction timerInteraction;
        private Presenter presenter;
        private PictureStorage pictureStorage;
        private FileStorage fileStorage;
        private Inclusion inclusion;

        public MultiscaleModelingApp()
        {
            InitializeComponent();
            InitializeComboBox();
            InitializeUserExpirience();

            memoryCache = MemoryCache.Instance;
            userInteraction = new UserInteraction();
            timerInteraction = new TimerInteraction();
            presenter = new Presenter();
            pictureStorage = new PictureStorage();
            fileStorage = new FileStorage();
            inclusion = new Inclusion();

        }

        private void InitializeComboBox()
        {
            inclusionComboBox.Items.Add("Circle");
            inclusionComboBox.Items.Add("Square");
            typeOfSimulationComboBox.Items.Add("Moore");
            typeOfSimulationComboBox.Items.Add("4 Rule");
        }

        private void InitializeUserExpirience()
        {
            generateSpaceButton.Enabled = false;
            generateNucleiButton.Enabled = false;
            simulationButton.Enabled = false;
            addInclusionButton.Enabled = false;
        }

        #region Basic Functionality
        private void TimerTick(object sender, EventArgs e)
        {
            var typeOfSimulaiton = typeOfSimulationComboBox.SelectedIndex;
            int.TryParse(probabilityTextBox.Text, out int probability);
            timerInteraction.TimerTicking(typeOfSimulaiton, probability);
            bool ifend = presenter.Draw(out Bitmap bitmap);
            pictureBox.Image = bitmap;
            if (ifend == true)
            {
                timer.Stop();
                simulationButton.Text = "Start";
            }
        }

        private void SimulationClick(object sender, EventArgs e)
        {
            if (!timer.Enabled)
            {
                timer.Start();
                simulationButton.Text = "Stop";
            }
            else
            {
                timer.Stop();
                simulationButton.Text = "Start";
            }
        }

        private void GenerateNucleiClick(object sender, EventArgs e)
        {
            int.TryParse(numberOfNucleiTextBox.Text, out int numberOfNucleis);
            userInteraction.GenerateNuclei(numberOfNucleis);
            pictureBox.Image = memoryCache.GetDisplayGraphic().Bitmap;

            simulationButton.Enabled = true;
        }

        private void GenerateSpaceClick(object sender, EventArgs e)
        {
            int.TryParse(sizeXTextBox.Text, out int sizex);
            int.TryParse(sizeYTextBox.Text, out int sizey);
            userInteraction.GenerateSpace(sizex, sizey);
            pictureBox.Size = new Size(sizex * 1, sizey * 1);
            pictureBox.Image = memoryCache.GetDisplayGraphic().Bitmap;

            enableGenerateNucleiButton = true;
            bool enable = !string.IsNullOrWhiteSpace(numberOfNucleiTextBox.Text) && enableGenerateNucleiButton;
            generateNucleiButton.Enabled = enable;
            addInclusionButton.Enabled = true;
        }

        private void MakeInclusionClick(object sender, EventArgs e)
        {
            int amountOfInclusions = 0;
            int sizeOfInclusions = 0;
            int inclusionsType = inclusionComboBox.SelectedIndex;
            bool result = Int32.TryParse(amountOfInclusionTextBox.Text, out amountOfInclusions);
            bool result2 = Int32.TryParse(sizeOfInclusionTextBox.Text, out sizeOfInclusions);
            inclusion.MakeInclusion(sizeOfInclusions, amountOfInclusions, inclusionsType);
            pictureBox.Image = memoryCache.GetDisplayGraphic().Bitmap;
        }
        #endregion

        #region Picture Storage
        private void ImportPictureClick(object sender, EventArgs e)
        {
            pictureStorage.ImportPicture();
            sizeYTextBox.Text = memoryCache.GetXSize().ToString();
            sizeXTextBox.Text = memoryCache.GetYSize().ToString();
            pictureBox.Image = memoryCache.GetDisplayGraphic().Bitmap;
        }

        private void SavePictureClick(object sender, EventArgs e)
        {
            pictureStorage.SavePicture(pictureBox);
        }
        #endregion

        #region File Storage
        private void ExportFileClick(object sender, EventArgs e)
        {
            fileStorage.SaveFile();
        }

        private void ImportFileClick(object sender, EventArgs e)
        {
            fileStorage.LoadFile();
            pictureBox.Image = memoryCache.GetDisplayGraphic().Bitmap;
        }
        #endregion

        #region UX
        private void SizeXTextBoxTextChanged(object sender, EventArgs e)
        {
            bool enable = 
                !string.IsNullOrWhiteSpace(sizeXTextBox.Text) && !string.IsNullOrWhiteSpace(sizeYTextBox.Text);
            generateSpaceButton.Enabled = enable;
        }

        private void SizeYTextBoxTextChanged(object sender, EventArgs e)
        {
            bool enable =
                !string.IsNullOrWhiteSpace(sizeXTextBox.Text) && !string.IsNullOrWhiteSpace(sizeYTextBox.Text);
            generateSpaceButton.Enabled = enable;
        }

        private void NumberOfNucleiTextBoxTextChanged(object sender, EventArgs e)
        {
            bool enable = !string.IsNullOrWhiteSpace(numberOfNucleiTextBox.Text) && enableGenerateNucleiButton;
            generateNucleiButton.Enabled = enable;
        }
        #endregion

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}