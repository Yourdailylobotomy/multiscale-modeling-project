﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace MultiscaleModeling
{
    public class Inclusion
    {

        private MemoryCache memoryCache;

        public Inclusion()
        {
            memoryCache = MemoryCache.Instance;
        }

        public void MakeInclusion(int sizeOfInlcusion, int amoutOfInclusion, int inclustionType)
        {
            bool flag = false;
            var borderList = BoarderList();
            var sizex = memoryCache.GetXSize();
            var sizey = memoryCache.GetYSize();
            var previouseMatrix = memoryCache.GetMatrixContainer().PreviouseMatrix;

            int i = 0, j = 0;
            var bitmap = memoryCache.GetDisplayGraphic().Bitmap;
            var listOfCells = new List<Cell>();
            var random = new Random();
            var graphics = Graphics.FromImage(bitmap);
            var mybrush = new SolidBrush(Color.White);

            for (int k = 0; k < amoutOfInclusion; k++)
            {
                listOfCells.Clear();
                flag = true;
                int x = 0, y = 0;
                int index = random.Next(borderList.Count);
                if (borderList.Any())
                {
                    x = borderList[index].X;
                    y = borderList[index].Y;
                }
                else
                {
                    x = random.Next(sizex);
                    y = random.Next(sizey);
                }

                if (x - sizeOfInlcusion >= 0 && x + sizeOfInlcusion < sizex &&
                    y - sizeOfInlcusion >= 0 && y + sizeOfInlcusion < sizey)
                {

                    for (i = x - sizeOfInlcusion; i < x + sizeOfInlcusion; i++)
                        for (j = y - sizeOfInlcusion; j < y + sizeOfInlcusion; j++)
                        {
                            double d = 0;
                            if (inclustionType == 0)
                            {
                                d = CircleInclusion(i, j, x, y);
                                if (d < sizeOfInlcusion)
                                {
                                    previouseMatrix[i][j].X = i;
                                    previouseMatrix[i][j].Y = j;
                                    listOfCells.Add(previouseMatrix[i][j]);
                                }
                            }
                            if (inclustionType == 1)
                            {
                                d = SquareInclusion(sizeOfInlcusion);
                                if (d <= sizeOfInlcusion)
                                {
                                    previouseMatrix[i][j].X = i;
                                    previouseMatrix[i][j].Y = j;
                                    listOfCells.Add(previouseMatrix[i][j]);
                                }
                            }

                            }
                    foreach (var cell in listOfCells)
                        if (cell.State == -1)
                            flag = false;

                    if (flag == true)
                    {
                        foreach (var cell in listOfCells)
                        {
                            previouseMatrix[cell.X][cell.Y].State = -1;
                            previouseMatrix[cell.X][cell.Y].Color = Color.Black;
                            previouseMatrix[cell.X][cell.Y].Id = -1;
                        }
                    }
                    else k--;
                }
                else k--;

                graphics = Graphics.FromImage(bitmap);
                for (i = 0; i < sizex; i++)
                {
                    for (j = 0; j < sizey; j++)
                    {
                        if (previouseMatrix[i][j].State != 0)
                        {
                            mybrush.Color = previouseMatrix[i][j].Color;
                            graphics.FillRectangle(mybrush, (i) * 1, (j) * 1, 1, 1);
                        }
                    }
                }
                memoryCache.UpdateBitmap(bitmap);
            }
        }

        public double CircleInclusion(int i, int j, int x, int y)
        {
            return Math.Sqrt(Math.Pow((x - i), 2) + Math.Pow((y - j), 2));
        }

        public double SquareInclusion(int sizeOfInclusion)
        {
            return sizeOfInclusion;
        }

        private List<Cell> BoarderList()
        {
            var sizex = memoryCache.GetXSize();
            var sizey = memoryCache.GetYSize();
            var currentMatrix = memoryCache.GetMatrixContainer().CurrentMatrix;
            var listOfCells = new List<Cell>();
            bool flaga = false;

            for (int i = 1; i < sizex - 1; i++)
            {
                for (int j = 1; j < sizey - 1; j++)
                {
                    flaga = false;
                    if (currentMatrix[i - 1][j - 1].Id != currentMatrix[i][j].Id) flaga = true;
                    if (currentMatrix[i][j - 1].Id != currentMatrix[i][j].Id) flaga = true;
                    if (currentMatrix[i + 1][j - 1].Id != currentMatrix[i][j].Id) flaga = true;

                    if (currentMatrix[i - 1][j].Id != currentMatrix[i][j].Id) flaga = true;
                    if (currentMatrix[i + 1][j].Id != currentMatrix[i][j].Id) flaga = true;

                    if (currentMatrix[i - 1][j + 1].Id != currentMatrix[i][j].Id) flaga = true;
                    if (currentMatrix[i][j + 1].Id != currentMatrix[i][j].Id) flaga = true;
                    if (currentMatrix[i + 1][j + 1].Id != currentMatrix[i][j].Id) flaga = true;
                    if (flaga == true)
                    {
                        currentMatrix[i][j].X = i;
                        currentMatrix[i][j].Y = j;
                        listOfCells.Add(currentMatrix[i][j]);
                    }
                }
            }

            return listOfCells;
        }
    }
}