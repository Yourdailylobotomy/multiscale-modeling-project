﻿using System.Drawing;

namespace MultiscaleModeling
{
    public class MemoryCache
    {
        private static MemoryCache instance = null;

        private MatrixContainer matrixContainer;
        private Board board;
        private DisplayGraphic displayGraphic;

        public static MemoryCache Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new MemoryCache();
                    instance.matrixContainer = new MatrixContainer();
                    instance.board = new Board();
                    instance.displayGraphic = new DisplayGraphic();
                }
                return instance;
            }
        }

        public static void Reset()
        {
            instance.matrixContainer = new MatrixContainer();
            instance.board = new Board();
            instance.displayGraphic = new DisplayGraphic();
        }

        public void UpdateMatrixContainer(MatrixContainer matrixContainer)
        {
            this.matrixContainer = matrixContainer;
        }

        public void UpdatePreviouseMatrix(Cell[][] previouseMatrix)
        {
            matrixContainer.PreviouseMatrix = previouseMatrix;
        }

        public void UpdateCurrentMatrix(Cell[][] currentMatrix)
        {
            matrixContainer.CurrentMatrix = currentMatrix;
        }

        public void UpdateBoard(Board board)
        {
            this.board = board;
        }

        public void UpdateXSize(int xSize)
        {
            if (board == null)
            {
                board = new Board();
            }
            board.XSize = xSize;
        }

        public void UpdateYSize(int ySize)
        {
            if (board == null)
            {
                board = new Board();
            }
            board.YSize = ySize;
        }

        public void UpdateDisplayGraphic(DisplayGraphic displayGraphic)
        {
            this.displayGraphic = displayGraphic;
        }

        public void UpdateBitmap(Bitmap bitmap)
        {
            displayGraphic.Bitmap = bitmap;
        }

        public void UpdateGraphics(Graphics graphics)
        {
            displayGraphic.Graphics = graphics;
        }

        public MatrixContainer GetMatrixContainer()
        {
            return matrixContainer;
        }

        public Cell[][] GetPreviouseMatrix()
        {
            if (matrixContainer == null)
            {
                matrixContainer = new MatrixContainer();
            }
            return matrixContainer.PreviouseMatrix;
        }

        public Cell[][] GetCurrentMatrix()
        {
            if (matrixContainer == null)
            {
                matrixContainer = new MatrixContainer();
            }
            return matrixContainer.CurrentMatrix;
        }

        public Board GetBoard()
        {
            return board;
        }

        public int GetXSize()
        {
            return board.XSize;
        }

        public int GetYSize()
        {
            return board.YSize;
        }

        public DisplayGraphic GetDisplayGraphic()
        {
            return displayGraphic;
        }
    }
}