﻿using System;
using System.Collections.Generic;

namespace MultiscaleModeling
{
    public class TimerInteraction
    {
        private MemoryCache memoryCache;

        public TimerInteraction()
        {
            memoryCache = MemoryCache.Instance;
        }

        public void TimerTicking(int typeOfCollisions, int? probability)
        {
            var sizex = memoryCache.GetXSize();
            var sizey = memoryCache.GetYSize();
            var previouseMatrix = memoryCache.GetPreviouseMatrix();
            var currentMatrix = memoryCache.GetCurrentMatrix();
            var rnd = new Random();
            var checkedCell = new Cell();
            for (int x = 0; x < sizex; x++)
            {
                for (int y = 0; y < sizey; y++)
                {

                    if (previouseMatrix[x][y].State == 0)
                    {
                        if (typeOfCollisions == 0)
                            checkedCell = CheckCollision(x, y);
                        if (typeOfCollisions == 1)
                        {
                            checkedCell = CheckCollision1Rule(x, y);
                            if (checkedCell.State == 0)
                                checkedCell = CheckCollision2Rule(x, y);
                            if (checkedCell.State == 0)
                                checkedCell = CheckCollision3Rule(x, y);
                            if (checkedCell.State == 0 && probability != null)
                                if (probability >= rnd.Next(1, 101))
                                    checkedCell = CheckCollision(x, y);
                        }
                        currentMatrix[x][y].Id = checkedCell.Id;
                        currentMatrix[x][y].State = checkedCell.State;
                        currentMatrix[x][y].Color = checkedCell.Color;
                    }
                    else
                    {
                        currentMatrix[x][y].Id = previouseMatrix[x][y].Id;
                        currentMatrix[x][y].State = previouseMatrix[x][y].State;
                        currentMatrix[x][y].Color = previouseMatrix[x][y].Color;
                    }
                }
            }
            memoryCache.UpdateCurrentMatrix(currentMatrix);
        }

        private Cell CheckCollision(int x, int y)
        {
            var previouseMatrix = memoryCache.GetPreviouseMatrix();
            var sizex = memoryCache.GetXSize();
            var sizey = memoryCache.GetYSize();
            var listOfCells = new List<Cell>();
            var result = new Cell();

            if (x == 0 && y == 0)
            {
                if (previouseMatrix[sizex - 1][sizey - 1].State == 1) listOfCells.Add(previouseMatrix[sizex - 1][sizey - 1]);
                if (previouseMatrix[x][sizey - 1].State == 1) listOfCells.Add(previouseMatrix[x][sizey - 1]);
                if (previouseMatrix[x + 1][sizey - 1].State == 1) listOfCells.Add(previouseMatrix[x + 1][sizey - 1]);
                if (previouseMatrix[sizex - 1][y].State == 1) listOfCells.Add(previouseMatrix[sizex - 1][y]);
                if (previouseMatrix[sizex - 1][y + 1].State == 1) listOfCells.Add(previouseMatrix[sizex - 1][y + 1]);
                if (previouseMatrix[x][y + 1].State == 1) listOfCells.Add(previouseMatrix[x][y + 1]);
                if (previouseMatrix[x + 1][y + 1].State == 1) listOfCells.Add(previouseMatrix[x + 1][y + 1]);
                if (previouseMatrix[x + 1][y].State == 1) listOfCells.Add(previouseMatrix[x + 1][y]);
            }
            else if (x == sizex - 1 && y == 0)
            {
                if (previouseMatrix[0][sizey - 1].State == 1) listOfCells.Add(previouseMatrix[0][sizey - 1]);
                if (previouseMatrix[x][sizey - 1].State == 1) listOfCells.Add(previouseMatrix[x][sizey - 1]);
                if (previouseMatrix[x - 1][sizey - 1].State == 1) listOfCells.Add(previouseMatrix[x - 1][sizey - 1]);
                if (previouseMatrix[0][y].State == 1) listOfCells.Add(previouseMatrix[0][y]);
                if (previouseMatrix[0][y + 1].State == 1) listOfCells.Add(previouseMatrix[0][y + 1]);
                if (previouseMatrix[x][y + 1].State == 1) listOfCells.Add(previouseMatrix[x][y + 1]);
                if (previouseMatrix[x - 1][y + 1].State == 1) listOfCells.Add(previouseMatrix[x - 1][y + 1]);
                if (previouseMatrix[x - 1][y].State == 1) listOfCells.Add(previouseMatrix[x - 1][y]);
            }
            else if (x == sizex - 1 && y == sizey - 1)
            {
                if (previouseMatrix[0][0].State == 1) listOfCells.Add(previouseMatrix[0][0]);
                if (previouseMatrix[x][0].State == 1) listOfCells.Add(previouseMatrix[x][0]);
                if (previouseMatrix[x - 1][0].State == 1) listOfCells.Add(previouseMatrix[x - 1][0]);
                if (previouseMatrix[0][y].State == 1) listOfCells.Add(previouseMatrix[0][y]);
                if (previouseMatrix[0][y - 1].State == 1) listOfCells.Add(previouseMatrix[0][y - 1]);
                if (previouseMatrix[x][y - 1].State == 1) listOfCells.Add(previouseMatrix[x][y - 1]);
                if (previouseMatrix[x - 1][y - 1].State == 1) listOfCells.Add(previouseMatrix[x - 1][y - 1]);
                if (previouseMatrix[x - 1][y].State == 1) listOfCells.Add(previouseMatrix[x - 1][y]);
            }
            else if (x == 0 && y == sizey - 1)
            {
                if (previouseMatrix[sizex - 1][0].State == 1) listOfCells.Add(previouseMatrix[sizex - 1][0]);
                if (previouseMatrix[x][0].State == 1) listOfCells.Add(previouseMatrix[x][0]);
                if (previouseMatrix[x + 1][0].State == 1) listOfCells.Add(previouseMatrix[x + 1][0]);
                if (previouseMatrix[sizex - 1][y].State == 1) listOfCells.Add(previouseMatrix[sizex - 1][y]);
                if (previouseMatrix[sizex - 1][y - 1].State == 1) listOfCells.Add(previouseMatrix[sizex - 1][y - 1]);
                if (previouseMatrix[x][y - 1].State == 1) listOfCells.Add(previouseMatrix[x][y - 1]);
                if (previouseMatrix[x + 1][y - 1].State == 1) listOfCells.Add(previouseMatrix[x + 1][y - 1]);
                if (previouseMatrix[x + 1][y].State == 1) listOfCells.Add(previouseMatrix[x + 1][y]);
            }
            else if (x == 0)
            {
                if (previouseMatrix[x][y - 1].State == 1) listOfCells.Add(previouseMatrix[x][y - 1]);
                if (previouseMatrix[x + 1][y - 1].State == 1) listOfCells.Add(previouseMatrix[x + 1][y - 1]);
                if (previouseMatrix[x + 1][y].State == 1) listOfCells.Add(previouseMatrix[x + 1][y]);
                if (previouseMatrix[x + 1][y + 1].State == 1) listOfCells.Add(previouseMatrix[x + 1][y + 1]);
                if (previouseMatrix[x][y + 1].State == 1) listOfCells.Add(previouseMatrix[x][y + 1]);
                if (previouseMatrix[sizex - 1][y - 1].State == 1) listOfCells.Add(previouseMatrix[sizex - 1][y - 1]);
                if (previouseMatrix[sizex - 1][y].State == 1) listOfCells.Add(previouseMatrix[sizex - 1][y]);
                if (previouseMatrix[sizex - 1][+1].State == 1) listOfCells.Add(previouseMatrix[sizex - 1][y + 1]);
            }
            else if (x == sizex - 1)
            {
                if (previouseMatrix[x][y - 1].State == 1) listOfCells.Add(previouseMatrix[x][y - 1]);
                if (previouseMatrix[x - 1][y - 1].State == 1) listOfCells.Add(previouseMatrix[x - 1][y - 1]);
                if (previouseMatrix[x - 1][y].State == 1) listOfCells.Add(previouseMatrix[x - 1][y]);
                if (previouseMatrix[x - 1][y + 1].State == 1) listOfCells.Add(previouseMatrix[x - 1][y + 1]);
                if (previouseMatrix[x][y + 1].State == 1) listOfCells.Add(previouseMatrix[x][y + 1]);
                if (previouseMatrix[0][y - 1].State == 1) listOfCells.Add(previouseMatrix[0][y - 1]);
                if (previouseMatrix[0][y].State == 1) listOfCells.Add(previouseMatrix[0][y]);
                if (previouseMatrix[0][y + 1].State == 1) listOfCells.Add(previouseMatrix[0][y + 1]);
            }
            else if (y == 0)
            {
                if (previouseMatrix[x - 1][y].State == 1) listOfCells.Add(previouseMatrix[x - 1][y]);
                if (previouseMatrix[x - 1][y + 1].State == 1) listOfCells.Add(previouseMatrix[x - 1][y + 1]);
                if (previouseMatrix[x][y + 1].State == 1) listOfCells.Add(previouseMatrix[x][y + 1]);
                if (previouseMatrix[x + 1][y + 1].State == 1) listOfCells.Add(previouseMatrix[x + 1][y + 1]);
                if (previouseMatrix[x + 1][y].State == 1) listOfCells.Add(previouseMatrix[x + 1][y]);
                if (previouseMatrix[x - 1][sizey - 1].State == 1) listOfCells.Add(previouseMatrix[x - 1][sizey - 1]);
                if (previouseMatrix[x][sizey - 1].State == 1) listOfCells.Add(previouseMatrix[x][sizey - 1]);
                if (previouseMatrix[x + 1][sizey - 1].State == 1) listOfCells.Add(previouseMatrix[x + 1][sizey - 1]);
            }
            else if (y == sizey - 1)
            {
                if (previouseMatrix[x - 1][y].State == 1) listOfCells.Add(previouseMatrix[x - 1][y]);
                if (previouseMatrix[x - 1][y - 1].State == 1) listOfCells.Add(previouseMatrix[x - 1][y - 1]);
                if (previouseMatrix[x][y - 1].State == 1) listOfCells.Add(previouseMatrix[x][y - 1]);
                if (previouseMatrix[x + 1][y - 1].State == 1) listOfCells.Add(previouseMatrix[x + 1][y - 1]);
                if (previouseMatrix[x + 1][y].State == 1) listOfCells.Add(previouseMatrix[x + 1][y]);
                if (previouseMatrix[x - 1][0].State == 1) listOfCells.Add(previouseMatrix[x - 1][0]);
                if (previouseMatrix[x][0].State == 1) listOfCells.Add(previouseMatrix[x][0]);
                if (previouseMatrix[x + 1][0].State == 1) listOfCells.Add(previouseMatrix[x + 1][0]);
            }
            else
            {
                if (previouseMatrix[x - 1][y - 1].State == 1) listOfCells.Add(previouseMatrix[x - 1][y - 1]);
                if (previouseMatrix[x][y - 1].State == 1) listOfCells.Add(previouseMatrix[x][y - 1]);
                if (previouseMatrix[x + 1][y - 1].State == 1) listOfCells.Add(previouseMatrix[x + 1][y - 1]);
                if (previouseMatrix[x - 1][y].State == 1) listOfCells.Add(previouseMatrix[x - 1][y]);
                if (previouseMatrix[x + 1][y].State == 1) listOfCells.Add(previouseMatrix[x + 1][y]);
                if (previouseMatrix[x - 1][y + 1].State == 1) listOfCells.Add(previouseMatrix[x - 1][y + 1]);
                if (previouseMatrix[x][y + 1].State == 1) listOfCells.Add(previouseMatrix[x][y + 1]);
                if (previouseMatrix[x + 1][y + 1].State == 1) listOfCells.Add(previouseMatrix[x + 1][y + 1]);
            }
            int most = 0;
            int only = 0;
            Cell m = new Cell();
            Cell o = new Cell();
            foreach (var zm in listOfCells)
            {
                m = zm;
                most = 0;
                foreach (var zm2 in listOfCells)
                {
                    if (zm2.Id == m.Id)
                    {
                        most++;
                    }
                    if (most > only)
                    {
                        only = most;
                        result = zm2;
                    }
                }

            }
            return result;
        }

        private Cell CheckCollision1Rule(int x, int y)
        {
            var previouseMatrix = memoryCache.GetPreviouseMatrix();
            var sizex = memoryCache.GetXSize();
            var sizey = memoryCache.GetYSize();
            var listOfCells = new List<Cell>();
            var result = new Cell();

            if (x == 0 && y == 0)
            {
                if (previouseMatrix[sizex - 1][sizey - 1].State == 1) listOfCells.Add(previouseMatrix[sizex - 1][sizey - 1]);
                if (previouseMatrix[x][sizey - 1].State == 1) listOfCells.Add(previouseMatrix[x][sizey - 1]);
                if (previouseMatrix[x + 1][sizey - 1].State == 1) listOfCells.Add(previouseMatrix[x + 1][sizey - 1]);
                if (previouseMatrix[sizex - 1][y].State == 1) listOfCells.Add(previouseMatrix[sizex - 1][y]);
                if (previouseMatrix[sizex - 1][y + 1].State == 1) listOfCells.Add(previouseMatrix[sizex - 1][y + 1]);
                if (previouseMatrix[x][y + 1].State == 1) listOfCells.Add(previouseMatrix[x][y + 1]);
                if (previouseMatrix[x + 1][y + 1].State == 1) listOfCells.Add(previouseMatrix[x + 1][y + 1]);
                if (previouseMatrix[x + 1][y].State == 1) listOfCells.Add(previouseMatrix[x + 1][y]);
            }
            else if (x == sizex - 1 && y == 0)
            {
                if (previouseMatrix[0][sizey - 1].State == 1) listOfCells.Add(previouseMatrix[0][sizey - 1]);
                if (previouseMatrix[x][sizey - 1].State == 1) listOfCells.Add(previouseMatrix[x][sizey - 1]);
                if (previouseMatrix[x - 1][sizey - 1].State == 1) listOfCells.Add(previouseMatrix[x - 1][sizey - 1]);
                if (previouseMatrix[0][y].State == 1) listOfCells.Add(previouseMatrix[0][y]);
                if (previouseMatrix[0][y + 1].State == 1) listOfCells.Add(previouseMatrix[0][y + 1]);
                if (previouseMatrix[x][y + 1].State == 1) listOfCells.Add(previouseMatrix[x][y + 1]);
                if (previouseMatrix[x - 1][y + 1].State == 1) listOfCells.Add(previouseMatrix[x - 1][y + 1]);
                if (previouseMatrix[x - 1][y].State == 1) listOfCells.Add(previouseMatrix[x - 1][y]);
            }
            else if (x == sizex - 1 && y == sizey - 1)
            {
                if (previouseMatrix[0][0].State == 1) listOfCells.Add(previouseMatrix[0][0]);
                if (previouseMatrix[x][0].State == 1) listOfCells.Add(previouseMatrix[x][0]);
                if (previouseMatrix[x - 1][0].State == 1) listOfCells.Add(previouseMatrix[x - 1][0]);
                if (previouseMatrix[0][y].State == 1) listOfCells.Add(previouseMatrix[0][y]);
                if (previouseMatrix[0][y - 1].State == 1) listOfCells.Add(previouseMatrix[0][y - 1]);
                if (previouseMatrix[x][y - 1].State == 1) listOfCells.Add(previouseMatrix[x][y - 1]);
                if (previouseMatrix[x - 1][y - 1].State == 1) listOfCells.Add(previouseMatrix[x - 1][y - 1]);
                if (previouseMatrix[x - 1][y].State == 1) listOfCells.Add(previouseMatrix[x - 1][y]);
            }
            else if (x == 0 && y == sizey - 1)
            {
                if (previouseMatrix[sizex - 1][0].State == 1) listOfCells.Add(previouseMatrix[sizex - 1][0]);
                if (previouseMatrix[x][0].State == 1) listOfCells.Add(previouseMatrix[x][0]);
                if (previouseMatrix[x + 1][0].State == 1) listOfCells.Add(previouseMatrix[x + 1][0]);
                if (previouseMatrix[sizex - 1][y].State == 1) listOfCells.Add(previouseMatrix[sizex - 1][y]);
                if (previouseMatrix[sizex - 1][y - 1].State == 1) listOfCells.Add(previouseMatrix[sizex - 1][y - 1]);
                if (previouseMatrix[x][y - 1].State == 1) listOfCells.Add(previouseMatrix[x][y - 1]);
                if (previouseMatrix[x + 1][y - 1].State == 1) listOfCells.Add(previouseMatrix[x + 1][y - 1]);
                if (previouseMatrix[x + 1][y].State == 1) listOfCells.Add(previouseMatrix[x + 1][y]);
            }
            else if (x == 0)
            {
                if (previouseMatrix[x][y - 1].State == 1) listOfCells.Add(previouseMatrix[x][y - 1]);
                if (previouseMatrix[x + 1][y - 1].State == 1) listOfCells.Add(previouseMatrix[x + 1][y - 1]);
                if (previouseMatrix[x + 1][y].State == 1) listOfCells.Add(previouseMatrix[x + 1][y]);
                if (previouseMatrix[x + 1][y + 1].State == 1) listOfCells.Add(previouseMatrix[x + 1][y + 1]);
                if (previouseMatrix[x][y + 1].State == 1) listOfCells.Add(previouseMatrix[x][y + 1]);
                if (previouseMatrix[sizex - 1][y - 1].State == 1) listOfCells.Add(previouseMatrix[sizex - 1][y - 1]);
                if (previouseMatrix[sizex - 1][y].State == 1) listOfCells.Add(previouseMatrix[sizex - 1][y]);
                if (previouseMatrix[sizex - 1][+1].State == 1) listOfCells.Add(previouseMatrix[sizex - 1][y + 1]);
            }
            else if (x == sizex - 1)
            {
                if (previouseMatrix[x][y - 1].State == 1) listOfCells.Add(previouseMatrix[x][y - 1]);
                if (previouseMatrix[x - 1][y - 1].State == 1) listOfCells.Add(previouseMatrix[x - 1][y - 1]);
                if (previouseMatrix[x - 1][y].State == 1) listOfCells.Add(previouseMatrix[x - 1][y]);
                if (previouseMatrix[x - 1][y + 1].State == 1) listOfCells.Add(previouseMatrix[x - 1][y + 1]);
                if (previouseMatrix[x][y + 1].State == 1) listOfCells.Add(previouseMatrix[x][y + 1]);
                if (previouseMatrix[0][y - 1].State == 1) listOfCells.Add(previouseMatrix[0][y - 1]);
                if (previouseMatrix[0][y].State == 1) listOfCells.Add(previouseMatrix[0][y]);
                if (previouseMatrix[0][y + 1].State == 1) listOfCells.Add(previouseMatrix[0][y + 1]);
            }
            else if (y == 0)
            {
                if (previouseMatrix[x - 1][y].State == 1) listOfCells.Add(previouseMatrix[x - 1][y]);
                if (previouseMatrix[x - 1][y + 1].State == 1) listOfCells.Add(previouseMatrix[x - 1][y + 1]);
                if (previouseMatrix[x][y + 1].State == 1) listOfCells.Add(previouseMatrix[x][y + 1]);
                if (previouseMatrix[x + 1][y + 1].State == 1) listOfCells.Add(previouseMatrix[x + 1][y + 1]);
                if (previouseMatrix[x + 1][y].State == 1) listOfCells.Add(previouseMatrix[x + 1][y]);
                if (previouseMatrix[x - 1][sizey - 1].State == 1) listOfCells.Add(previouseMatrix[x - 1][sizey - 1]);
                if (previouseMatrix[x][sizey - 1].State == 1) listOfCells.Add(previouseMatrix[x][sizey - 1]);
                if (previouseMatrix[x + 1][sizey - 1].State == 1) listOfCells.Add(previouseMatrix[x + 1][sizey - 1]);
            }
            else if (y == sizey - 1)
            {
                if (previouseMatrix[x - 1][y].State == 1) listOfCells.Add(previouseMatrix[x - 1][y]);
                if (previouseMatrix[x - 1][y - 1].State == 1) listOfCells.Add(previouseMatrix[x - 1][y - 1]);
                if (previouseMatrix[x][y - 1].State == 1) listOfCells.Add(previouseMatrix[x][y - 1]);
                if (previouseMatrix[x + 1][y - 1].State == 1) listOfCells.Add(previouseMatrix[x + 1][y - 1]);
                if (previouseMatrix[x + 1][y].State == 1) listOfCells.Add(previouseMatrix[x + 1][y]);
                if (previouseMatrix[x - 1][0].State == 1) listOfCells.Add(previouseMatrix[x - 1][0]);
                if (previouseMatrix[x][0].State == 1) listOfCells.Add(previouseMatrix[x][0]);
                if (previouseMatrix[x + 1][0].State == 1) listOfCells.Add(previouseMatrix[x + 1][0]);
            }
            else
            {
                if (previouseMatrix[x - 1][y - 1].State == 1) listOfCells.Add(previouseMatrix[x - 1][y - 1]);
                if (previouseMatrix[x][y - 1].State == 1) listOfCells.Add(previouseMatrix[x][y - 1]);
                if (previouseMatrix[x + 1][y - 1].State == 1) listOfCells.Add(previouseMatrix[x + 1][y - 1]);
                if (previouseMatrix[x - 1][y].State == 1) listOfCells.Add(previouseMatrix[x - 1][y]);
                if (previouseMatrix[x + 1][y].State == 1) listOfCells.Add(previouseMatrix[x + 1][y]);
                if (previouseMatrix[x - 1][y + 1].State == 1) listOfCells.Add(previouseMatrix[x - 1][y + 1]);
                if (previouseMatrix[x][y + 1].State == 1) listOfCells.Add(previouseMatrix[x][y + 1]);
                if (previouseMatrix[x + 1][y + 1].State == 1) listOfCells.Add(previouseMatrix[x + 1][y + 1]);
            }
            int most = 0;
            Cell m = new Cell();
            foreach (var zm in listOfCells)
            {
                m = zm;
                most = 0;
                foreach (var zm2 in listOfCells)
                {
                    if (zm2.Id == m.Id)
                    {
                        most++;
                    }
                    if (most >= 5)
                    {
                        result = zm2;
                    }
                }

            }
            return result;
        }

        private Cell CheckCollision2Rule(int x, int y)
        {
            var previouseMatrix = memoryCache.GetPreviouseMatrix();
            var sizex = memoryCache.GetXSize();
            var sizey = memoryCache.GetYSize();
            var listOfCells = new List<Cell>();
            var result = new Cell();

            if (x == 0 && y == 0)
            {
                if (previouseMatrix[sizex - 1][sizey - 1].State == 1) listOfCells.Add(previouseMatrix[sizex - 1][sizey - 1]);
                if (previouseMatrix[x][sizey - 1].State == 1) listOfCells.Add(previouseMatrix[x][sizey - 1]);
                if (previouseMatrix[x + 1][sizey - 1].State == 1) listOfCells.Add(previouseMatrix[x + 1][sizey - 1]);
                if (previouseMatrix[sizex - 1][y].State == 1) listOfCells.Add(previouseMatrix[sizex - 1][y]);
                if (previouseMatrix[sizex - 1][y + 1].State == 1) listOfCells.Add(previouseMatrix[sizex - 1][y + 1]);
                if (previouseMatrix[x][y + 1].State == 1) listOfCells.Add(previouseMatrix[x][y + 1]);
                if (previouseMatrix[x + 1][y + 1].State == 1) listOfCells.Add(previouseMatrix[x + 1][y + 1]);
                if (previouseMatrix[x + 1][y].State == 1) listOfCells.Add(previouseMatrix[x + 1][y]);
            }
            else if (x == sizex - 1 && y == 0)
            {
                if (previouseMatrix[0][sizey - 1].State == 1) listOfCells.Add(previouseMatrix[0][sizey - 1]);
                if (previouseMatrix[x][sizey - 1].State == 1) listOfCells.Add(previouseMatrix[x][sizey - 1]);
                if (previouseMatrix[x - 1][sizey - 1].State == 1) listOfCells.Add(previouseMatrix[x - 1][sizey - 1]);
                if (previouseMatrix[0][y].State == 1) listOfCells.Add(previouseMatrix[0][y]);
                if (previouseMatrix[0][y + 1].State == 1) listOfCells.Add(previouseMatrix[0][y + 1]);
                if (previouseMatrix[x][y + 1].State == 1) listOfCells.Add(previouseMatrix[x][y + 1]);
                if (previouseMatrix[x - 1][y + 1].State == 1) listOfCells.Add(previouseMatrix[x - 1][y + 1]);
                if (previouseMatrix[x - 1][y].State == 1) listOfCells.Add(previouseMatrix[x - 1][y]);
            }
            else if (x == sizex - 1 && y == sizey - 1)
            {
                if (previouseMatrix[0][0].State == 1) listOfCells.Add(previouseMatrix[0][0]);
                if (previouseMatrix[x][0].State == 1) listOfCells.Add(previouseMatrix[x][0]);
                if (previouseMatrix[x - 1][0].State == 1) listOfCells.Add(previouseMatrix[x - 1][0]);
                if (previouseMatrix[0][y].State == 1) listOfCells.Add(previouseMatrix[0][y]);
                if (previouseMatrix[0][y - 1].State == 1) listOfCells.Add(previouseMatrix[0][y - 1]);
                if (previouseMatrix[x][y - 1].State == 1) listOfCells.Add(previouseMatrix[x][y - 1]);
                if (previouseMatrix[x - 1][y - 1].State == 1) listOfCells.Add(previouseMatrix[x - 1][y - 1]);
                if (previouseMatrix[x - 1][y].State == 1) listOfCells.Add(previouseMatrix[x - 1][y]);
            }
            else if (x == 0 && y == sizey - 1)
            {
                if (previouseMatrix[sizex - 1][0].State == 1) listOfCells.Add(previouseMatrix[sizex - 1][0]);
                if (previouseMatrix[x][0].State == 1) listOfCells.Add(previouseMatrix[x][0]);
                if (previouseMatrix[x + 1][0].State == 1) listOfCells.Add(previouseMatrix[x + 1][0]);
                if (previouseMatrix[sizex - 1][y].State == 1) listOfCells.Add(previouseMatrix[sizex - 1][y]);
                if (previouseMatrix[sizex - 1][y - 1].State == 1) listOfCells.Add(previouseMatrix[sizex - 1][y - 1]);
                if (previouseMatrix[x][y - 1].State == 1) listOfCells.Add(previouseMatrix[x][y - 1]);
                if (previouseMatrix[x + 1][y - 1].State == 1) listOfCells.Add(previouseMatrix[x + 1][y - 1]);
                if (previouseMatrix[x + 1][y].State == 1) listOfCells.Add(previouseMatrix[x + 1][y]);
            }
            else if (x == 0)
            {
                if (previouseMatrix[x][y - 1].State == 1) listOfCells.Add(previouseMatrix[x][y - 1]);
                if (previouseMatrix[x + 1][y - 1].State == 1) listOfCells.Add(previouseMatrix[x + 1][y - 1]);
                if (previouseMatrix[x + 1][y].State == 1) listOfCells.Add(previouseMatrix[x + 1][y]);
                if (previouseMatrix[x + 1][y + 1].State == 1) listOfCells.Add(previouseMatrix[x + 1][y + 1]);
                if (previouseMatrix[x][y + 1].State == 1) listOfCells.Add(previouseMatrix[x][y + 1]);
                if (previouseMatrix[sizex - 1][y - 1].State == 1) listOfCells.Add(previouseMatrix[sizex - 1][y - 1]);
                if (previouseMatrix[sizex - 1][y].State == 1) listOfCells.Add(previouseMatrix[sizex - 1][y]);
                if (previouseMatrix[sizex - 1][+1].State == 1) listOfCells.Add(previouseMatrix[sizex - 1][y + 1]);
            }
            else if (x == sizex - 1)
            {
                if (previouseMatrix[x][y - 1].State == 1) listOfCells.Add(previouseMatrix[x][y - 1]);
                if (previouseMatrix[x - 1][y - 1].State == 1) listOfCells.Add(previouseMatrix[x - 1][y - 1]);
                if (previouseMatrix[x - 1][y].State == 1) listOfCells.Add(previouseMatrix[x - 1][y]);
                if (previouseMatrix[x - 1][y + 1].State == 1) listOfCells.Add(previouseMatrix[x - 1][y + 1]);
                if (previouseMatrix[x][y + 1].State == 1) listOfCells.Add(previouseMatrix[x][y + 1]);
                if (previouseMatrix[0][y - 1].State == 1) listOfCells.Add(previouseMatrix[0][y - 1]);
                if (previouseMatrix[0][y].State == 1) listOfCells.Add(previouseMatrix[0][y]);
                if (previouseMatrix[0][y + 1].State == 1) listOfCells.Add(previouseMatrix[0][y + 1]);
            }
            else if (y == 0)
            {
                if (previouseMatrix[x - 1][y].State == 1) listOfCells.Add(previouseMatrix[x - 1][y]);
                if (previouseMatrix[x - 1][y + 1].State == 1) listOfCells.Add(previouseMatrix[x - 1][y + 1]);
                if (previouseMatrix[x][y + 1].State == 1) listOfCells.Add(previouseMatrix[x][y + 1]);
                if (previouseMatrix[x + 1][y + 1].State == 1) listOfCells.Add(previouseMatrix[x + 1][y + 1]);
                if (previouseMatrix[x + 1][y].State == 1) listOfCells.Add(previouseMatrix[x + 1][y]);
                if (previouseMatrix[x - 1][sizey - 1].State == 1) listOfCells.Add(previouseMatrix[x - 1][sizey - 1]);
                if (previouseMatrix[x][sizey - 1].State == 1) listOfCells.Add(previouseMatrix[x][sizey - 1]);
                if (previouseMatrix[x + 1][sizey - 1].State == 1) listOfCells.Add(previouseMatrix[x + 1][sizey - 1]);
            }
            else if (y == sizey - 1)
            {
                if (previouseMatrix[x - 1][y].State == 1) listOfCells.Add(previouseMatrix[x - 1][y]);
                if (previouseMatrix[x - 1][y - 1].State == 1) listOfCells.Add(previouseMatrix[x - 1][y - 1]);
                if (previouseMatrix[x][y - 1].State == 1) listOfCells.Add(previouseMatrix[x][y - 1]);
                if (previouseMatrix[x + 1][y - 1].State == 1) listOfCells.Add(previouseMatrix[x + 1][y - 1]);
                if (previouseMatrix[x + 1][y].State == 1) listOfCells.Add(previouseMatrix[x + 1][y]);
                if (previouseMatrix[x - 1][0].State == 1) listOfCells.Add(previouseMatrix[x - 1][0]);
                if (previouseMatrix[x][0].State == 1) listOfCells.Add(previouseMatrix[x][0]);
                if (previouseMatrix[x + 1][0].State == 1) listOfCells.Add(previouseMatrix[x + 1][0]);
            }
            else
            {
                if (previouseMatrix[x][y - 1].State == 1) listOfCells.Add(previouseMatrix[x][y - 1]);
                if (previouseMatrix[x - 1][y].State == 1) listOfCells.Add(previouseMatrix[x - 1][y]);
                if (previouseMatrix[x + 1][y].State == 1) listOfCells.Add(previouseMatrix[x + 1][y]);
                if (previouseMatrix[x][y + 1].State == 1) listOfCells.Add(previouseMatrix[x][y + 1]);
            }
            int most = 0;
            Cell m = new Cell();
            Cell o = new Cell();
            foreach (var zm in listOfCells)
            {
                m = zm;
                most = 0;
                foreach (var zm2 in listOfCells)
                {
                    if (zm2.Id == m.Id)
                    {
                        most++;
                    }
                    if (most >= 3)
                    {
                        result = zm2;
                    }
                }

            }
            return result;
        }

        private Cell CheckCollision3Rule(int x, int y)
        {
            var previouseMatrix = memoryCache.GetPreviouseMatrix();
            var sizex = memoryCache.GetXSize();
            var sizey = memoryCache.GetYSize();
            var listOfCells = new List<Cell>();
            var result = new Cell();

            if (x == 0 && y == 0)
            {
                if (previouseMatrix[sizex - 1][sizey - 1].State == 1) listOfCells.Add(previouseMatrix[sizex - 1][sizey - 1]);
                if (previouseMatrix[x][sizey - 1].State == 1) listOfCells.Add(previouseMatrix[x][sizey - 1]);
                if (previouseMatrix[x + 1][sizey - 1].State == 1) listOfCells.Add(previouseMatrix[x + 1][sizey - 1]);
                if (previouseMatrix[sizex - 1][y].State == 1) listOfCells.Add(previouseMatrix[sizex - 1][y]);
                if (previouseMatrix[sizex - 1][y + 1].State == 1) listOfCells.Add(previouseMatrix[sizex - 1][y + 1]);
                if (previouseMatrix[x][y + 1].State == 1) listOfCells.Add(previouseMatrix[x][y + 1]);
                if (previouseMatrix[x + 1][y + 1].State == 1) listOfCells.Add(previouseMatrix[x + 1][y + 1]);
                if (previouseMatrix[x + 1][y].State == 1) listOfCells.Add(previouseMatrix[x + 1][y]);
            }
            else if (x == sizex - 1 && y == 0)
            {
                if (previouseMatrix[0][sizey - 1].State == 1) listOfCells.Add(previouseMatrix[0][sizey - 1]);
                if (previouseMatrix[x][sizey - 1].State == 1) listOfCells.Add(previouseMatrix[x][sizey - 1]);
                if (previouseMatrix[x - 1][sizey - 1].State == 1) listOfCells.Add(previouseMatrix[x - 1][sizey - 1]);
                if (previouseMatrix[0][y].State == 1) listOfCells.Add(previouseMatrix[0][y]);
                if (previouseMatrix[0][y + 1].State == 1) listOfCells.Add(previouseMatrix[0][y + 1]);
                if (previouseMatrix[x][y + 1].State == 1) listOfCells.Add(previouseMatrix[x][y + 1]);
                if (previouseMatrix[x - 1][y + 1].State == 1) listOfCells.Add(previouseMatrix[x - 1][y + 1]);
                if (previouseMatrix[x - 1][y].State == 1) listOfCells.Add(previouseMatrix[x - 1][y]);
            }
            else if (x == sizex - 1 && y == sizey - 1)
            {
                if (previouseMatrix[0][0].State == 1) listOfCells.Add(previouseMatrix[0][0]);
                if (previouseMatrix[x][0].State == 1) listOfCells.Add(previouseMatrix[x][0]);
                if (previouseMatrix[x - 1][0].State == 1) listOfCells.Add(previouseMatrix[x - 1][0]);
                if (previouseMatrix[0][y].State == 1) listOfCells.Add(previouseMatrix[0][y]);
                if (previouseMatrix[0][y - 1].State == 1) listOfCells.Add(previouseMatrix[0][y - 1]);
                if (previouseMatrix[x][y - 1].State == 1) listOfCells.Add(previouseMatrix[x][y - 1]);
                if (previouseMatrix[x - 1][y - 1].State == 1) listOfCells.Add(previouseMatrix[x - 1][y - 1]);
                if (previouseMatrix[x - 1][y].State == 1) listOfCells.Add(previouseMatrix[x - 1][y]);
            }
            else if (x == 0 && y == sizey - 1)
            {
                if (previouseMatrix[sizex - 1][0].State == 1) listOfCells.Add(previouseMatrix[sizex - 1][0]);
                if (previouseMatrix[x][0].State == 1) listOfCells.Add(previouseMatrix[x][0]);
                if (previouseMatrix[x + 1][0].State == 1) listOfCells.Add(previouseMatrix[x + 1][0]);
                if (previouseMatrix[sizex - 1][y].State == 1) listOfCells.Add(previouseMatrix[sizex - 1][y]);
                if (previouseMatrix[sizex - 1][y - 1].State == 1) listOfCells.Add(previouseMatrix[sizex - 1][y - 1]);
                if (previouseMatrix[x][y - 1].State == 1) listOfCells.Add(previouseMatrix[x][y - 1]);
                if (previouseMatrix[x + 1][y - 1].State == 1) listOfCells.Add(previouseMatrix[x + 1][y - 1]);
                if (previouseMatrix[x + 1][y].State == 1) listOfCells.Add(previouseMatrix[x + 1][y]);
            }
            else if (x == 0)
            {
                if (previouseMatrix[x][y - 1].State == 1) listOfCells.Add(previouseMatrix[x][y - 1]);
                if (previouseMatrix[x + 1][y - 1].State == 1) listOfCells.Add(previouseMatrix[x + 1][y - 1]);
                if (previouseMatrix[x + 1][y].State == 1) listOfCells.Add(previouseMatrix[x + 1][y]);
                if (previouseMatrix[x + 1][y + 1].State == 1) listOfCells.Add(previouseMatrix[x + 1][y + 1]);
                if (previouseMatrix[x][y + 1].State == 1) listOfCells.Add(previouseMatrix[x][y + 1]);
                if (previouseMatrix[sizex - 1][y - 1].State == 1) listOfCells.Add(previouseMatrix[sizex - 1][y - 1]);
                if (previouseMatrix[sizex - 1][y].State == 1) listOfCells.Add(previouseMatrix[sizex - 1][y]);
                if (previouseMatrix[sizex - 1][+1].State == 1) listOfCells.Add(previouseMatrix[sizex - 1][y + 1]);
            }
            else if (x == sizex - 1)
            {
                if (previouseMatrix[x][y - 1].State == 1) listOfCells.Add(previouseMatrix[x][y - 1]);
                if (previouseMatrix[x - 1][y - 1].State == 1) listOfCells.Add(previouseMatrix[x - 1][y - 1]);
                if (previouseMatrix[x - 1][y].State == 1) listOfCells.Add(previouseMatrix[x - 1][y]);
                if (previouseMatrix[x - 1][y + 1].State == 1) listOfCells.Add(previouseMatrix[x - 1][y + 1]);
                if (previouseMatrix[x][y + 1].State == 1) listOfCells.Add(previouseMatrix[x][y + 1]);
                if (previouseMatrix[0][y - 1].State == 1) listOfCells.Add(previouseMatrix[0][y - 1]);
                if (previouseMatrix[0][y].State == 1) listOfCells.Add(previouseMatrix[0][y]);
                if (previouseMatrix[0][y + 1].State == 1) listOfCells.Add(previouseMatrix[0][y + 1]);
            }
            else if (y == 0)
            {
                if (previouseMatrix[x - 1][y].State == 1) listOfCells.Add(previouseMatrix[x - 1][y]);
                if (previouseMatrix[x - 1][y + 1].State == 1) listOfCells.Add(previouseMatrix[x - 1][y + 1]);
                if (previouseMatrix[x][y + 1].State == 1) listOfCells.Add(previouseMatrix[x][y + 1]);
                if (previouseMatrix[x + 1][y + 1].State == 1) listOfCells.Add(previouseMatrix[x + 1][y + 1]);
                if (previouseMatrix[x + 1][y].State == 1) listOfCells.Add(previouseMatrix[x + 1][y]);
                if (previouseMatrix[x - 1][sizey - 1].State == 1) listOfCells.Add(previouseMatrix[x - 1][sizey - 1]);
                if (previouseMatrix[x][sizey - 1].State == 1) listOfCells.Add(previouseMatrix[x][sizey - 1]);
                if (previouseMatrix[x + 1][sizey - 1].State == 1) listOfCells.Add(previouseMatrix[x + 1][sizey - 1]);
            }
            else if (y == sizey - 1)
            {
                if (previouseMatrix[x - 1][y].State == 1) listOfCells.Add(previouseMatrix[x - 1][y]);
                if (previouseMatrix[x - 1][y - 1].State == 1) listOfCells.Add(previouseMatrix[x - 1][y - 1]);
                if (previouseMatrix[x][y - 1].State == 1) listOfCells.Add(previouseMatrix[x][y - 1]);
                if (previouseMatrix[x + 1][y - 1].State == 1) listOfCells.Add(previouseMatrix[x + 1][y - 1]);
                if (previouseMatrix[x + 1][y].State == 1) listOfCells.Add(previouseMatrix[x + 1][y]);
                if (previouseMatrix[x - 1][0].State == 1) listOfCells.Add(previouseMatrix[x - 1][0]);
                if (previouseMatrix[x][0].State == 1) listOfCells.Add(previouseMatrix[x][0]);
                if (previouseMatrix[x + 1][0].State == 1) listOfCells.Add(previouseMatrix[x + 1][0]);
            }
            else
            {
                if (previouseMatrix[x - 1][y - 1].State == 1) listOfCells.Add(previouseMatrix[x - 1][y - 1]);
                if (previouseMatrix[x + 1][y - 1].State == 1) listOfCells.Add(previouseMatrix[x + 1][y - 1]);
                if (previouseMatrix[x - 1][y + 1].State == 1) listOfCells.Add(previouseMatrix[x - 1][y + 1]);
                if (previouseMatrix[x + 1][y + 1].State == 1) listOfCells.Add(previouseMatrix[x + 1][y + 1]);
            }
            int most = 0;
            Cell m = new Cell();
            Cell o = new Cell();
            foreach (var zm in listOfCells)
            {
                m = zm;
                most = 0;
                foreach (var zm2 in listOfCells)
                {
                    if (zm2.Id == m.Id)
                    {
                        most++;
                    }
                    if (most >= 3)
                    {
                        result = zm2;
                    }
                }

            }
            return result;
        }
    }
}