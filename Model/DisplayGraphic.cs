﻿using System.Drawing;

namespace MultiscaleModeling
{
    public class DisplayGraphic
    {
        public Graphics Graphics { get; set; }
        public Bitmap Bitmap { get; set; }
    }
}