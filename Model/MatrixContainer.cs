﻿using System.Drawing;

namespace MultiscaleModeling
{
    public class MatrixContainer
    {
        public Cell[][] PreviouseMatrix { get; set; }
        public Cell[][] CurrentMatrix { get; set; }
        public Cell[][] HelperMatrix { get; set; }

        public MatrixContainer()
        {

        }

        public MatrixContainer(int xSize, int ySize)
        {
            PreviouseMatrix = new Cell[xSize][];
            CurrentMatrix = new Cell[xSize][];
            HelperMatrix = new Cell[xSize][];
            for (int xCoordinate = 0; xCoordinate < xSize; xCoordinate++)
            {
                PreviouseMatrix[xCoordinate] = new Cell[ySize];
                CurrentMatrix[xCoordinate] = new Cell[ySize];
                HelperMatrix[xCoordinate] = new Cell[ySize];
                for (int yCoordinate = 0; yCoordinate < ySize; yCoordinate++)
                {
                    PreviouseMatrix[xCoordinate][yCoordinate] = new Cell(0, 0, Color.White, xCoordinate, yCoordinate);
                    CurrentMatrix[xCoordinate][yCoordinate] = new Cell(0, 0, Color.White, xCoordinate, yCoordinate);
                    HelperMatrix[xCoordinate][yCoordinate] = new Cell(0, 0, Color.White, xCoordinate, yCoordinate);
                }
            }
        }
    }
}