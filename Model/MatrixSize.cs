﻿namespace MultiscaleModeling
{
    public class Board
    {
        public int XSize { get; set; }
        public int YSize { get; set; }
        public int Size { get; set; }

        public Board()
        {
            XSize = 0;
            YSize = 0;
        }

        public Board(int xSize, int ySize)
        {
            XSize = xSize;
            YSize = ySize;
        }
    }
}