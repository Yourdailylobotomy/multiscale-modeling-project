﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace MultiscaleModeling
{
    public class FileStorage
    {
        private MemoryCache memoryCache;

        public FileStorage()
        {
            memoryCache = MemoryCache.Instance;
        }

        public void SaveFile()
        {
            var saveDialog = new SaveFileDialog();
            saveDialog.FileName = "microstructure";
            saveDialog.DefaultExt = "txt";
            saveDialog.Filter = "txt file (*.txt)|*.txt";

            var sizex = memoryCache.GetXSize();
            var sizey = memoryCache.GetYSize();
            var previouseMatrix = memoryCache.GetPreviouseMatrix();

            if (saveDialog.ShowDialog() == DialogResult.OK)
            {
                var streamWriter = new StreamWriter(saveDialog.FileName.ToString(), true);
                streamWriter.WriteLine(sizex + " " + sizey);

                for (int i = 0; i < sizex; i++)
                {
                    for (int j = 0; j < sizey; j++)
                    {
                        streamWriter.WriteLine(i + " " + j + " " + previouseMatrix[i][j].State + " " + previouseMatrix[i][j].Id);
                    }
                }
                streamWriter.Flush();
                streamWriter.Close();
            }
        }

        public void LoadFile()
        {
            var openFileDialog = new OpenFileDialog();
            var result = openFileDialog.ShowDialog();

            if (result == DialogResult.OK)
            {
                var streamReader = new StreamReader(openFileDialog.FileName.ToString(), true);
                var random = new Random();
                var colorIds = new Dictionary<int, Color>();
                var line = streamReader.ReadLine();

                string[] xy = line.Split(' ');

                var sizex = int.Parse(xy[0]);
                var sizey = int.Parse(xy[1]);

                sizex++;
                sizey++;

                var matrixContainer = new MatrixContainer(sizex, sizey);

                while ((line = streamReader.ReadLine()) != null)
                {

                    xy = line.Split(' ');
                    int x = int.Parse(xy[0]);
                    int y = int.Parse(xy[1]);
                    int key = int.Parse(xy[3]);
                    if (key != 0)
                    {
                        matrixContainer.PreviouseMatrix[x][y].Id = key;
                        matrixContainer.PreviouseMatrix[x][y].State = 1;
                        if (colorIds.ContainsKey(key)) matrixContainer.PreviouseMatrix[x][y].Color = colorIds[key];
                        else
                        {

                            Color tmpcolor = Color.FromArgb((byte)random.Next(0, 255), (byte)random.Next(0, 255), (byte)random.Next(0, 255));
                            colorIds.Add(key, tmpcolor);
                            matrixContainer.PreviouseMatrix[x][y].Color = tmpcolor;
                        }
                    }
                    else
                    {
                        matrixContainer.PreviouseMatrix[x][y].Id = 0;
                        matrixContainer.PreviouseMatrix[x][y].State = 0;
                        matrixContainer.PreviouseMatrix[x][y].Color = Color.White;
                    }

                }

                var bitmap = new Bitmap(1 * sizex, 1 * sizey);
                var graphics = Graphics.FromImage(bitmap);
                var mybrush = new SolidBrush(Color.White);

                graphics.Clear(Color.White);
                for (int i = 0; i < sizex; i++)
                {
                    for (int j = 0; j < sizey; j++)
                    {
                        if (matrixContainer.PreviouseMatrix[i][j].State == 1)
                        {
                            mybrush.Color = matrixContainer.PreviouseMatrix[i][j].Color;
                            graphics.FillRectangle(mybrush, i * 1, j * 1, 1, 1);
                        }
                    }
                }

                memoryCache.UpdateXSize(sizex);
                memoryCache.UpdateYSize(sizey);
                memoryCache.UpdateMatrixContainer(matrixContainer);
                memoryCache.UpdateGraphics(graphics);
                memoryCache.UpdateBitmap(bitmap);
            }
        }
    }
}