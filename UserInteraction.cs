﻿using System;
using System.Drawing;

namespace MultiscaleModeling
{
    public class UserInteraction
    {
        private MemoryCache memoryCache;
        public UserInteraction()
        {
            memoryCache = MemoryCache.Instance;
        }

        public void GenerateSpace(int sizex, int sizey)
        {
            MemoryCache.Reset();

            var matrixContainer = new MatrixContainer(sizex, sizey);
            var bitmap = new Bitmap(1 * sizex, 1 * sizey);
            var graphics = Graphics.FromImage(bitmap);
            var mybrush = new SolidBrush(Color.White);

            graphics.Clear(Color.White);
            for (int i = 0; i < sizex; i++)
            {
                for (int j = 0; j < sizey; j++)
                {
                    if (matrixContainer.PreviouseMatrix[i][j].State == 1)
                    {
                        mybrush.Color = matrixContainer.PreviouseMatrix[i][j].Color;
                        graphics.FillRectangle(mybrush, i * 1, j * 1, 1, 1);
                    }
                }
            }

            memoryCache.UpdateXSize(sizex);
            memoryCache.UpdateYSize(sizey);
            memoryCache.UpdateMatrixContainer(matrixContainer);
            memoryCache.UpdateBitmap(bitmap);
            memoryCache.UpdateGraphics(graphics);
        }

        public void GenerateNuclei(int numberOfNucleis = 0)
        {
            var sizex = memoryCache.GetXSize();
            var sizey = memoryCache.GetYSize();
            var previouseMatrix = memoryCache.GetPreviouseMatrix();
            var graphics = memoryCache.GetDisplayGraphic().Graphics;
            var mybrush = new SolidBrush(Color.White);

            GetRandomPoints(numberOfNucleis);

            graphics.Clear(Color.White);
            for (int i = 0; i < sizex; i++)
            {
                for (int j = 0; j < sizey; j++)
                {
                    if (previouseMatrix[i][j].State != 0)
                    {
                        mybrush.Color = previouseMatrix[i][j].Color;
                        graphics.FillRectangle(mybrush, i * 1, j * 1, 1, 1);
                    }
                }
            }

            memoryCache.UpdateGraphics(graphics);
        }

        private void GetRandomPoints(int numberOfNucleis)
        {
            var sizex = memoryCache.GetXSize();
            var sizey = memoryCache.GetYSize();
            var previousMatrix = memoryCache.GetPreviouseMatrix();
            var random = new Random();

            int id = 0;
            for (int i = 0; i < numberOfNucleis; i++)
            {
                int randomX = 0;
                int randomY = 0;

                randomX = random.Next(0, sizex - 1);
                randomY = random.Next(0, sizey - 1);

                if (previousMatrix[randomX][randomY].State == 0)
                {
                    id++;
                    previousMatrix[randomX][randomY].State = 1;
                    previousMatrix[randomX][randomY].Id = id;
                    previousMatrix[randomX][randomY].Color = Color.FromArgb((byte)random.Next(0, 255), (byte)random.Next(0, 255), (byte)random.Next(0, 255));
                }
                else i--;
            }

            memoryCache.UpdatePreviouseMatrix(previousMatrix);
        }
    }
}