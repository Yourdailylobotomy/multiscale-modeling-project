﻿using System.Drawing;

namespace MultiscaleModeling
{
    public class Presenter
    {
        private MemoryCache memoryCache;

        public Presenter()
        {
            memoryCache = MemoryCache.Instance;
        }

        public bool Draw(out Bitmap bitmap)
        {
            var sizex = memoryCache.GetXSize();
            var sizey = memoryCache.GetYSize();
            var currentMatrix = memoryCache.GetCurrentMatrix();
            var previousMatrix = memoryCache.GetPreviouseMatrix();
            bitmap = memoryCache.GetDisplayGraphic().Bitmap;
            var graphics = Graphics.FromImage(bitmap);
            var mybrush = new SolidBrush(Color.White);

            bool ifend = true;
            for (int i = 0; i < sizex; i++)
            {
                for (int j = 0; j < sizey; j++)
                {
                    if (currentMatrix[i][j].State == 1)
                    {
                        mybrush.Color = currentMatrix[i][j].Color;
                        graphics.FillRectangle(mybrush, (i) * 1, (j) * 1, 1, 1);
                        previousMatrix[i][j].Id = currentMatrix[i][j].Id;
                        previousMatrix[i][j].State = currentMatrix[i][j].State;
                        previousMatrix[i][j].Color = currentMatrix[i][j].Color;
                    }
                    else ifend = false;
                }
            }

            memoryCache.UpdateDisplayGraphic(
               new DisplayGraphic
               {
                   Bitmap = bitmap,
                   Graphics = graphics
               });
            memoryCache.UpdatePreviouseMatrix(previousMatrix);

            return ifend;
        }
    }
}
