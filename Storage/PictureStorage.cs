﻿using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace MultiscaleModeling
{
    public class PictureStorage
    {
        private MemoryCache memoryCache;

        public PictureStorage()
        {
            memoryCache = MemoryCache.Instance;
        }

        public void SavePicture(PictureBox pictureBox)
        {
            var saveDialog = new SaveFileDialog();

            saveDialog.FileName = "obraz";
            saveDialog.DefaultExt = "bmp";
            saveDialog.Filter = "BMP images (*.bmp)|*.bmp";

            if (saveDialog.ShowDialog() == DialogResult.OK)
            {
                pictureBox.Image.Save(saveDialog.FileName.ToString(), System.Drawing.Imaging.ImageFormat.Bmp);
            }
        }

        public void ImportPicture()
        {
            var openFileDialog = new OpenFileDialog();
            var result = openFileDialog.ShowDialog();

            if (result == DialogResult.OK)
            {
                var image = (Bitmap)Image.FromFile(openFileDialog.FileName.ToString(), true);

                var sizey = image.Height;
                var sizex = image.Width;

                var board = new Board(sizex, sizey);
                var matrixContainer = new MatrixContainer(board.XSize, board.YSize);
               
                var colorIds = new SortedDictionary<int, Color>();

                bool first = true;
                for (int i = 0; i < board.XSize; i++)
                {
                    for (int j = 0; j < board.YSize; j++)
                    {
                        var pixelcolor = image.GetPixel(i, j);
                        if (pixelcolor.R != 255 && pixelcolor.G != 255 && pixelcolor.B != 255)
                        {
                            if (colorIds.ContainsValue(pixelcolor))
                            {
                                int key = colorIds.FirstOrDefault(x => x.Value == pixelcolor).Key;
                                matrixContainer.PreviouseMatrix[i][j].Color = colorIds[key];
                                matrixContainer.PreviouseMatrix[i][j].State = 1;
                                matrixContainer.PreviouseMatrix[i][j].Id = key;
                            }
                            else
                            {
                                int keymax;
                                if (first)
                                {
                                    keymax = -1;
                                    first = false;
                                }
                                else keymax = colorIds.Keys.Last();
                                keymax++;
                                colorIds.Add(keymax, pixelcolor);
                                matrixContainer.PreviouseMatrix[i][j].Color = pixelcolor;
                                matrixContainer.PreviouseMatrix[i][j].State = 1;
                                matrixContainer.PreviouseMatrix[i][j].Id = keymax;
                            }
                        }
                        else
                        {
                            matrixContainer.PreviouseMatrix[i][j].State = 0;
                            matrixContainer.PreviouseMatrix[i][j].Id = 0;
                            matrixContainer.PreviouseMatrix[i][j].Color = Color.White;
                        }
                    }
                }

                var bitmap = new Bitmap(1 * sizex, 1 * sizey);
                var graphics = Graphics.FromImage(bitmap);
                var mybrush = new SolidBrush(Color.White);

                graphics.Clear(Color.White);
                for (int i = 0; i < sizex; i++)
                {
                    for (int j = 0; j < sizey; j++)
                    {
                        if (matrixContainer.PreviouseMatrix[i][j].State == 1)
                        {
                            mybrush.Color = matrixContainer.PreviouseMatrix[i][j].Color;
                            graphics.FillRectangle(mybrush, i * 1, j * 1, 1, 1);
                        }
                    }
                }

                memoryCache.UpdateXSize(sizex);
                memoryCache.UpdateYSize(sizey);
                memoryCache.UpdateMatrixContainer(matrixContainer);
                memoryCache.UpdateGraphics(graphics);
                memoryCache.UpdateBitmap(bitmap);
            }
        }
    }
}