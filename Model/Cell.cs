﻿using System.Drawing;

namespace MultiscaleModeling
{
    public class Cell
    {
        public int Id { get; set; }
        public Color Color { get; set; }
        public int State { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public int XCoordinate { get; set; }
        public int YCoordinate { get; set; }

        public Cell()
        {
            X = 0;
            Y = 0;
        }

        public Cell(int id, int state)
        {
            State = state;
            Id = id;
        }

        public Cell(int id, int state, Color color, int xCoordinate, int yCoordinate)
        {
            Id = id;
            State = state;
            Color = color;
            XCoordinate = xCoordinate;
            YCoordinate = yCoordinate;
            X = 0;
            Y = 0;
        }
    }
}