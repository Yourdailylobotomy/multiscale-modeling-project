﻿namespace MultiscaleModeling
{
    partial class MultiscaleModelingApp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pictureBox = new System.Windows.Forms.PictureBox();
            this.sizeXTextBox = new System.Windows.Forms.TextBox();
            this.sizeYTextBox = new System.Windows.Forms.TextBox();
            this.generateSpaceButton = new System.Windows.Forms.Button();
            this.numberOfNucleiTextBox = new System.Windows.Forms.TextBox();
            this.generateNucleiButton = new System.Windows.Forms.Button();
            this.simulationButton = new System.Windows.Forms.Button();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fILEToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.microstructureToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bitmapToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.savePictureToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.addInclusionButton = new System.Windows.Forms.Button();
            this.amountOfInclusionTextBox = new System.Windows.Forms.TextBox();
            this.sizeOfInclusionTextBox = new System.Windows.Forms.TextBox();
            this.inclusionComboBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.typeOfSimulationComboBox = new System.Windows.Forms.ComboBox();
            this.probabilityTextBox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox
            // 
            this.pictureBox.Location = new System.Drawing.Point(386, 66);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(432, 341);
            this.pictureBox.TabIndex = 0;
            this.pictureBox.TabStop = false;
            // 
            // sizeXTextBox
            // 
            this.sizeXTextBox.Location = new System.Drawing.Point(124, 82);
            this.sizeXTextBox.Name = "sizeXTextBox";
            this.sizeXTextBox.Size = new System.Drawing.Size(93, 20);
            this.sizeXTextBox.TabIndex = 1;
            this.sizeXTextBox.TextChanged += new System.EventHandler(this.SizeXTextBoxTextChanged);
            // 
            // sizeYTextBox
            // 
            this.sizeYTextBox.Location = new System.Drawing.Point(124, 118);
            this.sizeYTextBox.Name = "sizeYTextBox";
            this.sizeYTextBox.Size = new System.Drawing.Size(93, 20);
            this.sizeYTextBox.TabIndex = 2;
            this.sizeYTextBox.TextChanged += new System.EventHandler(this.SizeYTextBoxTextChanged);
            // 
            // generateSpaceButton
            // 
            this.generateSpaceButton.Location = new System.Drawing.Point(238, 82);
            this.generateSpaceButton.Name = "generateSpaceButton";
            this.generateSpaceButton.Size = new System.Drawing.Size(142, 56);
            this.generateSpaceButton.TabIndex = 3;
            this.generateSpaceButton.Text = "Generate space";
            this.generateSpaceButton.UseVisualStyleBackColor = true;
            this.generateSpaceButton.Click += new System.EventHandler(this.GenerateSpaceClick);
            // 
            // numberOfNucleiTextBox
            // 
            this.numberOfNucleiTextBox.Location = new System.Drawing.Point(124, 158);
            this.numberOfNucleiTextBox.Name = "numberOfNucleiTextBox";
            this.numberOfNucleiTextBox.Size = new System.Drawing.Size(93, 20);
            this.numberOfNucleiTextBox.TabIndex = 4;
            this.numberOfNucleiTextBox.TextChanged += new System.EventHandler(this.NumberOfNucleiTextBoxTextChanged);
            // 
            // generateNucleiButton
            // 
            this.generateNucleiButton.Location = new System.Drawing.Point(238, 156);
            this.generateNucleiButton.Name = "generateNucleiButton";
            this.generateNucleiButton.Size = new System.Drawing.Size(142, 23);
            this.generateNucleiButton.TabIndex = 5;
            this.generateNucleiButton.Text = "Nucleation";
            this.generateNucleiButton.UseVisualStyleBackColor = true;
            this.generateNucleiButton.Click += new System.EventHandler(this.GenerateNucleiClick);
            // 
            // simulationButton
            // 
            this.simulationButton.Location = new System.Drawing.Point(124, 202);
            this.simulationButton.Name = "simulationButton";
            this.simulationButton.Size = new System.Drawing.Size(256, 44);
            this.simulationButton.TabIndex = 6;
            this.simulationButton.Text = "Simulation";
            this.simulationButton.UseVisualStyleBackColor = true;
            this.simulationButton.Click += new System.EventHandler(this.SimulationClick);
            // 
            // timer
            // 
            this.timer.Tick += new System.EventHandler(this.TimerTick);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fILEToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1045, 24);
            this.menuStrip1.TabIndex = 7;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fILEToolStripMenuItem
            // 
            this.fILEToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.microstructureToolStripMenuItem,
            this.bitmapToolStripMenuItem});
            this.fILEToolStripMenuItem.Name = "fILEToolStripMenuItem";
            this.fILEToolStripMenuItem.Size = new System.Drawing.Size(40, 20);
            this.fILEToolStripMenuItem.Text = "FILE";
            // 
            // microstructureToolStripMenuItem
            // 
            this.microstructureToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.importToolStripMenuItem,
            this.exportToolStripMenuItem});
            this.microstructureToolStripMenuItem.Name = "microstructureToolStripMenuItem";
            this.microstructureToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.microstructureToolStripMenuItem.Text = "Microstructure";
            // 
            // importToolStripMenuItem
            // 
            this.importToolStripMenuItem.Name = "importToolStripMenuItem";
            this.importToolStripMenuItem.Size = new System.Drawing.Size(110, 22);
            this.importToolStripMenuItem.Text = "Import";
            this.importToolStripMenuItem.Click += new System.EventHandler(this.ImportFileClick);
            // 
            // exportToolStripMenuItem
            // 
            this.exportToolStripMenuItem.Name = "exportToolStripMenuItem";
            this.exportToolStripMenuItem.Size = new System.Drawing.Size(110, 22);
            this.exportToolStripMenuItem.Text = "Export";
            this.exportToolStripMenuItem.Click += new System.EventHandler(this.ExportFileClick);
            // 
            // bitmapToolStripMenuItem
            // 
            this.bitmapToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.savePictureToolStripMenuItem,
            this.importToolStripMenuItem1});
            this.bitmapToolStripMenuItem.Name = "bitmapToolStripMenuItem";
            this.bitmapToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.bitmapToolStripMenuItem.Text = "Bitmap";
            // 
            // savePictureToolStripMenuItem
            // 
            this.savePictureToolStripMenuItem.Name = "savePictureToolStripMenuItem";
            this.savePictureToolStripMenuItem.Size = new System.Drawing.Size(138, 22);
            this.savePictureToolStripMenuItem.Text = "Save picture";
            this.savePictureToolStripMenuItem.Click += new System.EventHandler(this.SavePictureClick);
            // 
            // importToolStripMenuItem1
            // 
            this.importToolStripMenuItem1.Name = "importToolStripMenuItem1";
            this.importToolStripMenuItem1.Size = new System.Drawing.Size(138, 22);
            this.importToolStripMenuItem1.Text = "Import";
            this.importToolStripMenuItem1.Click += new System.EventHandler(this.ImportPictureClick);
            // 
            // addInclusionButton
            // 
            this.addInclusionButton.Location = new System.Drawing.Point(238, 273);
            this.addInclusionButton.Name = "addInclusionButton";
            this.addInclusionButton.Size = new System.Drawing.Size(142, 115);
            this.addInclusionButton.TabIndex = 8;
            this.addInclusionButton.Text = "Add inclusions";
            this.addInclusionButton.UseVisualStyleBackColor = true;
            this.addInclusionButton.Click += new System.EventHandler(this.MakeInclusionClick);
            // 
            // amountOfInclusionTextBox
            // 
            this.amountOfInclusionTextBox.Location = new System.Drawing.Point(140, 273);
            this.amountOfInclusionTextBox.Name = "amountOfInclusionTextBox";
            this.amountOfInclusionTextBox.Size = new System.Drawing.Size(77, 20);
            this.amountOfInclusionTextBox.TabIndex = 9;
            // 
            // sizeOfInclusionTextBox
            // 
            this.sizeOfInclusionTextBox.Location = new System.Drawing.Point(140, 322);
            this.sizeOfInclusionTextBox.Name = "sizeOfInclusionTextBox";
            this.sizeOfInclusionTextBox.Size = new System.Drawing.Size(77, 20);
            this.sizeOfInclusionTextBox.TabIndex = 10;
            // 
            // inclusionComboBox
            // 
            this.inclusionComboBox.FormattingEnabled = true;
            this.inclusionComboBox.Location = new System.Drawing.Point(140, 367);
            this.inclusionComboBox.Name = "inclusionComboBox";
            this.inclusionComboBox.Size = new System.Drawing.Size(77, 21);
            this.inclusionComboBox.TabIndex = 11;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(68, 88);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 13);
            this.label1.TabIndex = 12;
            this.label1.Text = "Size X";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(68, 118);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 13);
            this.label2.TabIndex = 13;
            this.label2.Text = "Size Y";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(30, 165);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(75, 13);
            this.label3.TabIndex = 14;
            this.label3.Text = "Grains amount";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(30, 273);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(104, 13);
            this.label4.TabIndex = 15;
            this.label4.Text = "Amonut of inclusions";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(39, 325);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(88, 13);
            this.label5.TabIndex = 16;
            this.label5.Text = "Size of inclusions";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(30, 370);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(92, 13);
            this.label6.TabIndex = 17;
            this.label6.Text = "Type of inclusions";
            // 
            // typeOfSimulationComboBox
            // 
            this.typeOfSimulationComboBox.FormattingEnabled = true;
            this.typeOfSimulationComboBox.Location = new System.Drawing.Point(124, 44);
            this.typeOfSimulationComboBox.Name = "typeOfSimulationComboBox";
            this.typeOfSimulationComboBox.Size = new System.Drawing.Size(93, 21);
            this.typeOfSimulationComboBox.TabIndex = 18;
            // 
            // probabilityTextBox
            // 
            this.probabilityTextBox.Location = new System.Drawing.Point(307, 45);
            this.probabilityTextBox.Name = "probabilityTextBox";
            this.probabilityTextBox.Size = new System.Drawing.Size(73, 20);
            this.probabilityTextBox.TabIndex = 19;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(13, 52);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(92, 13);
            this.label7.TabIndex = 20;
            this.label7.Text = "Type of simulaiton";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(235, 52);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(55, 13);
            this.label8.TabIndex = 21;
            this.label8.Text = "Probability";
            // 
            // MultiscaleModelingApp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1045, 419);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.probabilityTextBox);
            this.Controls.Add(this.typeOfSimulationComboBox);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.inclusionComboBox);
            this.Controls.Add(this.sizeOfInclusionTextBox);
            this.Controls.Add(this.amountOfInclusionTextBox);
            this.Controls.Add(this.addInclusionButton);
            this.Controls.Add(this.simulationButton);
            this.Controls.Add(this.generateNucleiButton);
            this.Controls.Add(this.numberOfNucleiTextBox);
            this.Controls.Add(this.generateSpaceButton);
            this.Controls.Add(this.sizeYTextBox);
            this.Controls.Add(this.sizeXTextBox);
            this.Controls.Add(this.pictureBox);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MultiscaleModelingApp";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox;
        private System.Windows.Forms.TextBox sizeXTextBox;
        private System.Windows.Forms.TextBox sizeYTextBox;
        private System.Windows.Forms.Button generateSpaceButton;
        private System.Windows.Forms.TextBox numberOfNucleiTextBox;
        private System.Windows.Forms.Button generateNucleiButton;
        private System.Windows.Forms.Button simulationButton;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fILEToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem microstructureToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem importToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bitmapToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem savePictureToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem importToolStripMenuItem1;
        private System.Windows.Forms.Button addInclusionButton;
        private System.Windows.Forms.TextBox amountOfInclusionTextBox;
        private System.Windows.Forms.TextBox sizeOfInclusionTextBox;
        private System.Windows.Forms.ComboBox inclusionComboBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox typeOfSimulationComboBox;
        private System.Windows.Forms.TextBox probabilityTextBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
    }
}

